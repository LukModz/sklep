<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Product;

class ProductController extends Controller
{
    public function index()
    {
        $products = new Product;
        $queries = [];
        $columns = [
            'category',
            'sort',
            'minval',
            'maxval',
            'items',
        ];

        if(request()->has('category')){
            $products=$products->where('category', request('category'));
            $queries['category'] = request('category');
        }

        if(request()->has('sort')){
            if(request('sort')=='name'){
            $products=$products->OrderBy('name', 'asc');
            }else if(request('sort')=='priceasc'){
                $products=$products->OrderBy('price', 'asc');
            }else if(request('sort')=='pricedesc'){
                $products=$products->OrderBy('price', 'desc');
            }
            $queries['sort'] = request('sort');
        }

        if(request()->has('minval')&&!empty ( request('minval'))){
            $products=$products->where('price','>', request('minval'));
            $queries['minval'] = request('minval');
        }
        if(request()->has('maxval')&&!empty ( request('maxval'))){
            $products=$products->where('price','<', request('maxval'));
            $queries['maxval'] = request('maxval');
        }   
        if(request()->has('items')){
            $products = $products->paginate(request('items'));
            $queries['items'] = request('items');
        }else{
            $products = $products->paginate(12);
        }
        $products = $products->appends($queries);
        return view('shop')->with('products', $products);
    }

    public function getDetails($id)
    {
        return view('product-details', ['product' => Product::findOrFail($id)]);
    }
}