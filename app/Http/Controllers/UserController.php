<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DB;
use App\User;
use App\Product;
use App\Order;
use App\Cart;
use Session;

class UserController extends Controller
{

    public function getlogin()
    {
        return view('login');
    }

    public function postSignup(Request $request){
        $this->validate($request,[
            'email'=>'email|required|unique:users',
            'password'=>'required|min:4',
            'name'=>'required',
            'surname'=>'required',
            'city'=>'required',
            'street'=>'required',
        ]);
        $user = new User([
            'email' => $request->input('email'),
            'password'=>bcrypt($request->input('password')),
            'name'=>$request->input('name'),
            'surname'=>$request->input('surname'),
            'city'=>$request->input('city'),
            'street'=>$request->input('street'),
        ]);
        $user->save();

        Auth::login($user);

        return redirect()->route('product.index');
    }

    public function postSignin(Request $request){
        $this->validate($request,[
            'email'=>'email|required',
            'password'=>'required|min:4',
        ]);

        if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])){
            return redirect()->route('user.profile');
        }
        return redirect()->back();
    }

    public function getProfile(){

        $userid = Auth::user()->id;

        $orders = Order::where('user_id',$userid)->paginate(5);

        return view('profile', compact('orders'));
    }

    public function getLogout(){
        Auth::logout();
        return redirect()->route('product.index');
    }
}