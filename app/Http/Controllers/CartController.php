<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use DB;
use App\Product;
use App\Cart;
use Session;

class CartController extends Controller
{

    public function getCart(){
        if(!Session::has('cart')){
            return view('shopcart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $products = collect($cart->items);
        return view('shopcart', ['products'=>$products, 'totalPrice' => $cart->totalPrice]);
    }

    public function addToCart($id)
    {
        $product = Product::find($id);
        if(!$product) {

            abort(404);

        }
        $oldCart = session()->get('cart');
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);
        session()->put('cart',$cart);
        Session::save();
        return redirect()->back();
    }
    public function downQty($id)
    {
        $oldCart= session()->get('cart');
        $cart = new Cart($oldCart);
        if($cart->items[$id]['qty']>1)
        {
            $cart->decrementQty($id);
        }
        session()->put('cart',$cart);
        Session::save();
        return redirect()->back();
    }

    public function deleteItem($id)
    {
        $oldCart= session()->get('cart');
        $cart = new Cart($oldCart);
        $cart->deleteItem($id);
        session()->put('cart',$cart);
        Session::save();
        if($cart->totalQty == 0){
            Session::forget('cart');
        }
        return redirect()->back();
    }
}