<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use Auth;
use DB;
use App\Product;
use App\Cart;
use App\Order;
use Session;

class OrderController extends Controller
{
    public function getOrder($id){
        $order = Order::findorfail($id);
        $cart = new Cart(unserialize($order->cart));

        return view('order-details', ['order' => $order, 'cart'=>$cart, 'products'=>$cart->items]);
    }

    public function postOrder(Request $request){

        $this->validate($request,[
            'email'=>'email|required',
            'name'=>'required',
            'surname'=>'required',
            'city'=>'required',
            'street'=>'required',
        ]);
        if(!Session::has('cart')){
            return redirect()->route('cart.getCart');
        }
        $oldCart=Session::get('cart');
        $cart = new Cart($oldCart);

        if(empty($cart->items)){
            return redirect()->route('cart.getCart');
        }

        $order = new Order();
        
        $order->name = $request->input('name');
        $order->surname = $request->input('surname');
        $order->email = $request->input('email');
        $order->city = $request->input('city');
        $order->street = $request->input('street');
        $order->cart = serialize($cart);
        if(Auth::check()){
            $order->user_id = $request->user()->id;
        }
        Session::forget('cart');
        $order->save();
        return redirect()->route('product.index');
    }
}