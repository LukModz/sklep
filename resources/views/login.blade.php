@extends('layouts.master-layout')

@section('content')
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Zaloguj się do swojego konta</h2>
						@if(count($errors)>0)
							<div class="alert alert-danger">
								@foreach ($errors->all() as $error)
									<p>
										{{$error}}
									</p>
								@endforeach
							</div>
						@endif
						<form action="{{route('user.signin')}}" method="POST">
							<input type="email" name="email" placeholder="E-mail" />
							<input type="password" name="password" placeholder="Hasło"/>
							<button type="submit" class="btn btn-default">Zaloguj</button>
							{{ csrf_field() }}
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">lub</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Zarejestruj się!</h2>
						@if(count($errors)>0)
							<div class="alert alert-danger">
								@foreach ($errors->all() as $error)
									<p>
										{{$error}}
									</p>
								@endforeach
							</div>
						@endif
						<form action="{{route('user.signup')}}" method="POST">
							<input type="text" name="name" placeholder="Imię"/>
							<input type="text" name="surname" placeholder="Nazwisko"/>
							<input type="text" name="city" placeholder="Miasto"/>
							<input type="text" name="street" placeholder="Ulica"/>
							<input type="email" name="email" placeholder="E-mail"/>
							<input type="password" name="password" placeholder="Hasło"/>
							<button type="submit" class="btn btn-default">Zarejestruj</button>
							{{ csrf_field() }}
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
@endsection