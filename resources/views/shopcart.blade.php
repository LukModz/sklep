@extends('layouts.master-layout')

@section('content')

	<section id="cart_items">
		<div class="container">
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Przedmiot</td>
							<td class="description"></td>
							<td class="price">Cena</td>
							<td class="quantity">Ilość</td>
							<td class="total">Cena całkowita</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@if(Session::has('cart'))
							@foreach ($products as $product)
								<tr>
									<td class="cart_product">
										<a href={{route('product.details', ['id'=>$product['item']['id']])}}><img src="{{ URL::to('/') }}/images/{{ $product['item']['image']}}" alt="" />
									</td>
									<td class="cart_description">
										<h4><a href={{route('product.details', ['id'=>$product['item']['id']])}}>{{ $product['item']['name'] }}</a></h4>
									</td>
									<td class="cart_price">
										<p>${{ $product['item']['price'] }}</p>
									</td>
									<td class="cart_quantity" style="min-width:150px">
										<div class="cart_quantity_button">
											<a class="cart_quantity_up" href="{{route('cart.addToCart',['id'=>$product['item']->id])}}"> + </a>
											<input class="cart_quantity_input" type="text" name="quantity" value={{ $product['qty'] }} autocomplete="off" size="2" readonly>
											<a class="cart_quantity_down" href="{{route('cart.downQty',['id'=>$product['item']->id])}}"> - </a>
										</div>
									</td>
									<td class="cart_total">
										<p class="cart_total_price">${{ $product['price']}}</p>
									</td>
									<td class="cart_delete">
										<a class="cart_quantity_delete" href="{{route('cart.deleteItem',['id'=>$product['item']->id])}}"><i class="fa fa-times"></i></a>
									</td>
								</tr>
							@endforeach
							<tr>
								<td colspan="4">&nbsp;</td>
								<td colspan="2">
									<table class="table table-condensed total-result">
											<td>Cena końcowa: </td>
											<td><span>${{$totalPrice}}</span></td>
										</tr>
									</table>
								</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-sm-4">
		</div>
		<div class="col-sm-2">
			<div class="register-req">
				<p>Zarejstruj się i zaloguj, by móc przeglądać historię swoich zamówień lub złóż zamówienie jako gość.</p>
			</div><!--/register-req-->
		</div>
			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-3">
						<div class="shopper-info">
							<p>Dane klienta:</p>
							@if(count($errors)>0)
								<div class="alert alert-danger">
									@foreach ($errors->all() as $error)
										<p>
											{{$error}}
										</p>
									@endforeach
								</div>
							@endif
							<form action="{{route('order.purchase')}}" method="POST">
								<input type="text" name="name" placeholder="Imię" @if(Auth::check())value= "{{Auth::user()->name}}" @endif>
								<input type="text" name="surname" placeholder="Nazwisko" @if(Auth::check())value= "{{Auth::user()->surname}}" @endif>
								<input type="email" name="email" placeholder="E-mail" @if(Auth::check())value= "{{Auth::user()->email}}" @endif>
								<input type="text" name="city" placeholder="Miasto" @if(Auth::check())value= "{{Auth::user()->city}}" @endif>
								<input type="text" name="street" placeholder="Ulica" @if(Auth::check())value= "{{Auth::user()->street}}" @endif>
								<div align="right">
									<button type="submit" class="btn btn-primary">Zamów</button>
								</div>
								{{ csrf_field() }}
							</form>
						</div>
					</div>
				</div>
			</div>
		</div><br>
	</section> <!--/#cart_items-->
@endsection