@extends('layouts.master-layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Kategorie</h2>
                    <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="{{route('product.index')}}">Wszystkie</a></h4>
                                </div>
                            </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="{{route('product.index',['category'=>'football'])}}">Do piłki nożnej</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="{{route('product.index',['category'=>'basketball'])}}">Do koszykówki</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="{{route('product.index',['category'=>'running'])}}">Do biegania</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="{{route('product.index',['category'=>'trekking'])}}">Trekkingowe</a></h4>
                            </div>
                        </div>
                    </div><!--/category-products-->
                
                    <div class="brands_products"><!--brands_products-->
                        <h2>Sortuj</h2>
                        <div class="brands-name">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="{{route('product.index',['category'=>request('category'), 'minval'=>request('minval'), 'maxval'=>request('maxval'), 'sort'=>'name'])}}"> <span class="pull-right"></span>Alfabetycznie</a></li>
                                <li><a href="{{route('product.index',['category'=>request('category'), 'minval'=>request('minval'), 'maxval'=>request('maxval'), 'sort'=>'priceasc'])}}"> <span class="pull-right"></span>Cena od najmniejszej</a></li>
                                <li><a href="{{route('product.index',['category'=>request('category'), 'minval'=>request('minval'), 'maxval'=>request('maxval'), 'sort'=>'pricedesc'])}}"> <span class="pull-right"></span>Cena od najwiekszej</a></li>
                            </ul>
                        </div>
                    </div><!--/brands_products-->
                    
                    <div class="price-range" style="position: relative;">
                        <!--price-range-->
                        <h2>Zakres ceny</h2>
                        <form action="{{route('product.index')}}" method="GET" >
                                @if(request()->has('category'))
                                    <input type="hidden" name="category" value={{request('category')}}>
                                @endif
                                @if(request()->has('sort'))
                                    <input type="hidden" name="sort" value={{request('sort')}}>
                                @endif
                                <h5 style="text-align: center;">od: <input type="text" name="minval" size = 5></h5><br>   
                                <h5 style="text-align: center;">do: <input type="text" name="maxval" size = 5></h5><br>   
                            <input type="submit" class="btn btn-default add-to-cart" value="szukaj" style="display:inherit; position: relative;margin-left:auto; margin-right:auto;" >
                        </form>
                    </div><!--/price-range-->
                    
                    <div class="shipping text-center"><!--shipping-->
                        <img src="images/home/shipping.jpg" alt="" />
                    </div><!--/shipping-->
                
                </div>
            </div>
            <div class="col-sm-9 padding-right">
                <div class="features_items">
                    @yield('contents')
                </div>
            </div>
        </div>
    </div>
@endsection