@extends('layouts.shop-layout')

@section('contents')
	<div class="header-bottom">
		<div class="col-sm-12">
			<ul class="nav nav-tabs">
				<li><h5>pokaż: </h5></li>
				<li><a href={{route('product.index',['category'=>request('category'),'sort'=>request('sort'),'minval'=>request('minval'),'maxval'=>request('maxval'),'items'=>'12'])}} style="color:black">12</a></li>
				<li><a href={{route('product.index',['category'=>request('category'),'sort'=>request('sort'),'minval'=>request('minval'),'maxval'=>request('maxval'),'items'=>'24'])}} style="color:black">24</a></li>
				<li><a href={{route('product.index',['category'=>request('category'),'sort'=>request('sort'),'minval'=>request('minval'),'maxval'=>request('maxval'),'items'=>'36'])}} style="color:black">36</a></li>
			</ul>
		</div>
	</div>
	<div class="tab-content">
		<div class="tab-pane fade active in" >
			@foreach ($products as $product)
				<div class="col-sm-3">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<a href ={{route('product.details', [$product->id])}}>
									<img src="{{ URL::to('/') }}/images/{{ $product->image }}" alt="" />
									<h2>${{ $product->price }}</h2>
									<li>{{ str_limit( $product->name, $limit = 17, $end = '...') }}</li>
								</a>
								<a href="{{route('cart.addToCart', ['id' =>$product->id])}}" class="btn btn-default add-to-cart">
									<i class="fa fa-shopping-cart"></i>Do koszyka
								</a>
								</div>						
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
	<div style="display: flex;flex-flow: column wrap;align-items: center;">
		{{ $products->links() }}
	</div>
@endsection
