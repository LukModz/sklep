@extends('layouts.shop-layout')

@section('contents')
	<section>
		<div class="col-sm-9 padding-right">
			<div class="product-details"><!--product-details-->
				<div class="col-sm-5">
					<div class="product-information"><!--/product-information-->
						<img src="images/product-details/new.jpg" class="newarrival" alt="" />
						<h2>{{$product->name}}</h2>
						<p>ID produktu: {{$product->id}}</p>
						<h1>${{$product->price}}</h1>
						<span>
							<a href="{{route('cart.addToCart', ['id' =>$product->id])}}" class="btn btn-default add-to-cart">
								<i class="fa fa-shopping-cart"></i>Do koszyka
							</a>
						</span>
						<p><b>Dostępność:</b> W magazynie</p>
					</div><!--/product-information-->
				</div>
				<div class="col-sm-7">
					<div class="view-product">
						<img src="{{ URL::to('/') }}/images/{{ $product->image }}" alt="" />
					</div>
				</div>
			</div><!--/product-details-->
			<div class="tab-pane fade active in" id="reviews" >
				<div class="col-sm-12">
					<h4>Opis produktu:</h4>
					<p>{{$product->description}}</p>
				</div>
			</div>
		</div><!--/category-tab-->	
	</section>
@endsection