@extends('layouts.master-layout')

@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Przedmiot</td>
							<td class="description"></td>
							<td class="price">Cena</td>
							<td class="quantity">Ilość</td>
							<td class="total">Cena całkowita</td>
						</tr>
					</thead>
					<tbody>
						@foreach ($products as $product)
							<tr>
								<td class="cart_product">
									<a href={{route('product.details', ['id'=>$product['item']['id']])}}><img src="{{ URL::to('/') }}/images/{{ $product['item']['image']}}" alt="" />
								</td>
								<td class="cart_description">
									<h4><a href={{route('product.details', ['id'=>$product['item']['id']])}}>{{ $product['item']['name'] }}</a></h4>
								</td>
								<td class="cart_price">
									<p>${{ $product['item']['price'] }}</p>
								</td>
								<td class="cart_quantity" style="min-width:150px">
									<div class="cart_quantity_button">
										<input class="cart_quantity_input" type="text" name="quantity" value={{ $product['qty'] }} autocomplete="off" size="2" readonly>
									</div>
								</td>
								<td class="cart_total">
									<p class="cart_total_price">${{ $product['price']}}</p>
								</td>
							</tr>
						@endforeach
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
										<td>Cena końcowa: </td>
										<td><span>${{$cart->totalPrice}}</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
        </div>

         <div class="shopper-info" align="center">
                <p>Dane kupującego:</p>
                <p>Imie:     {{$order->name}}</p><br>
                <p>Nazwisko: {{$order->surname}}</p><br>
                <p>E-mial:   {{$order->email}} </p><br>
                <p>Miasto:   {{$order->city}}</p><br>
                <p>Ulica:    {{$order->street}}</p><br>
                    <p>Data złożenia zamówienia:    {{$order->created_at}}</p><br>
		</div><br>
	</section> <!--/#cart_items-->
@endsection