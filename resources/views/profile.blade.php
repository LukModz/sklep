
@extends('layouts.master-layout')

@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="">Zamówienie</td>
							<td class="">Data</td>
							<td class="total">Cena</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						@foreach ($orders as $order)			
							<tr>
								<td class="">
									<a href={{route('order.details',['id'=>$order->order_id])}}>
										<p>ID zamówienia: {{$order->order_id}}</p>
									</a>
								</td>	
								<td class="">
									<p>{{$order->created_at}}</p>
								</td>
								<td class="cart_total">
									<p class="cart_total_price">${{unserialize($order->cart)->totalPrice}}</p>
								</td>	
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div style="display: flex;flex-flow: column wrap;align-items: center;">
				{{ $orders->links() }}
			</div>
		</div>
	</section> <!--/#cart_items-->
	@endsection