<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    

Route::get('/cart', "CartController@getCart")->name('cart.getCart');
Route::get("/add/{id}", "CartController@addToCart")->name('cart.addToCart');
Route::get("/down/{id}", "CartController@downQty")->name('cart.downQty');
Route::get("/delete/{id}", "CartController@deleteItem")->name('cart.deleteItem');

Route::get('order/{id}','OrderController@getOrder')->name('order.details');
Route::post('/purchase', 'OrderController@postOrder')->name('order.purchase');

Route::get('/', "ProductController@index")->name('product.index');
Route::get("/product/{id}", "ProductController@getDetails")->name('product.details');

Route::group(['prefix'=>'user'],function(){
    Route::group(['middleware' => 'guest'],function(){
        Route::get("/login", "UserController@getlogin")->name('user.login');
        Route::post("/signup", "UserController@postSignup")->name('user.signup');
        Route::post("/signin", "UserController@postSignin")->name('user.signin');
    });
    Route::group(['middleware' => 'auth'],function(){
        Route::get("/profile", "UserController@getProfile")->name('user.profile');
        Route::get('/logout', 'UserController@getLogout')->name('user.logout');
    });
 
});


